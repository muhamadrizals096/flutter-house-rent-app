import 'package:flutter/material.dart';
import 'package:flutter_house_rent_app/screen/home/widget/best_offer.dart';
import 'package:flutter_house_rent_app/screen/home/widget/categories.dart';
import 'package:flutter_house_rent_app/screen/home/widget/custom_app_bar.dart';
import 'package:flutter_house_rent_app/screen/home/widget/custom_bottom_navigation_bar.dart';
import 'package:flutter_house_rent_app/screen/home/widget/recommended_house.dart';
import 'package:flutter_house_rent_app/screen/home/widget/search_input.dart';
import 'package:flutter_house_rent_app/screen/home/widget/welcome_text.dart';

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: CustomAppbar(),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            WelcomeText(),
            SearchInput(),
            Categories(),
            RecommendedHouse(),
            BestOffer(),
          ],
        ),
      ),
      bottomNavigationBar: CustomBottomNavigationBar(),
    );
  }
}

class CustomBottomNavigationBAr {
}