import 'package:flutter/material.dart';

class About extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('About',
            style: Theme.of(context).textTheme.headline1!.
            copyWith(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 6),
          Text('Enum veniam dolor sit ipsum culpa magna dolor incidiunt laborim exceptum...',
          style: Theme.of(context).textTheme.bodyText1!.
            copyWith(
              fontSize: 14,
            ),
          ),
        ],
      ),
    );
  }
}